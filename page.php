<?php get_header(); ?>
<section id="contentArea">
<?php get_sidebar(); ?>
<div id="contentWrapper">
<article id="mainContent" class="contentBox">
	<?php if(is_front_page()) { ?>
		<div id="homeContent" class="contentBox">
	<?php } ?>
		<?php if ( have_posts() ) :
			while ( have_posts() ) :
				the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
<?php get_footer(); ?>