<?php wp_footer(); ?>
		<?php if(is_front_page()) { ?>
		</div><!-- homeContent -->
		<div id="complianceText" class="contentBox">
			<h2>Important Implications to Consider</h2>
			<p>QuickCashAdvance.com only works with lenders who give customers detailed information on terms and conditions prior to their acceptance of the loan offer. It is recommended that you closely review the terms of any loan offer presented to you. For more details about the aforementioned considerations see our <a href="/privacy-policy/">Lending Policy</a> and <a href="/rates-fees/">Rates & Fees</a> sections.</p>
			<div id="notaccordion">
			<h3>Financial implications (Interest and finance charges)</h3>
			<div>
			<p>
			If you are approved for a loan, your lender will present you with the exact fees and interest rate of your loan prior to your formally accepting their offer. CashAdvance.org is not a lender and cannot predict the exact fees and interest that will be attached to the loan offer presented to you. You are not under any obligation whatsoever to accept the terms that the lender presents to you.</p>
			</div>
			<h3>Implications of non-payment</h3>
			<div>
			<p>
			When you accept the terms and conditions for a particular loan offer, you are agreeing to pay back the loan principal and finance charges in the amount of time specified in the documents that your lender supplies. Additional charges or fees may apply in the event that you are not able to repay your loan in full or make a late payment. CashAdvance.org cannot predict the amount of the charges or fees that you will incur as a result of partial payment, late payment or nonpayment. CashAdvance.org has no control over knowledge of the loan details between you and your lender. Please refer to the partial payment, late payment and nonpayment policies detailed in the loan documents that your lender provides you with. CashAdvance.org does make an effort to work only with reputable lenders who are committed to pursuing collections of past-due accounts in a reasonable and fair manner.</p>
			</div>
			<h3>Potential impact to credit score</h3>
			<div>
			<p>
			CashAdvance.org does not make credit decisions nor does it ever conduct credit inquiries on potential borrowers. Some members of the CashAdvance.org lender network may choose to conduct a nontraditional credit check in order to determine your eligibility for a loan. Lenders typically will not conduct a credit inquiry with any of the three major credit reporting agencies (Transunion, Experian and Equifax). Ultimately, your credit score may be affected by the actions of a particular lender. If you do not repay your loan on time your lender may report this delinquency to one or more credit reporting agencies, which could have a negative impact on your credit score. CashAdvance.org encourages consumers with credit problems to consult a Credit Counseling company.</p>
			</div>
			<h3>Collection Practices</h3>
			<div>
			<p>
			CashAdvance.org is not a lender and does not engage in debt collection practices. Your lender will disclose their collection practices to you in their loan documents. If you are not sure of the collection practices used by a specific lender we recommend that you discuss this issue with the lender directly. CashAdvance.org makes reasonable efforts to only work with established, reputable lenders who pursue collections of past-due accounts in a fair manner.</p>
			</div>
			<h3>Loan Renewal Policies</h3>
			<div>
			<p>
			Loan renewal policies are largely governed by state regulatory laws. Loan renewal options will be presented to you by your lender prior to your acceptance of their loan offer. Please be sure to carefully read the renewal policy presented to you before you sign the loan documents. Cash loans are intended to be a short-term financial instrument. CashAdvance.org encourages all borrowers to repay their loan in full and on time in order to avoid nonpayment and/or late payment fees. If you think that you may be unable to pay off a cash loan after taking it out, we recommend that you explore Loan Alternatives before applying for a loan.</p>
			</div>
		</div>
		<?php } ?>
	</article>
	</div>
</section><!-- contentArea -->
<footer id="mainFooter">
	<div id="fLeft">
		<?php wp_nav_menu(array('theme_location' => 'footer-nav', 'container' => false, 'items_wrap' => '<ul>%3$s</ul>')); ?>
		<p>&copy; 2013 QuickCashAdvance.com. <span>All Rights Reserved.</span></p>
	</div><!-- fLeft -->	
	<a href="/" id="logo-footer"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-footer.png" alt="Quick Cash Advance" /></a>
</footer>
</div><!-- container --></div><!-- wrapperHome --></div><!-- wrapper -->
</body></html>