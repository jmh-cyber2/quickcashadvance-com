<?php session_start(); ?>
<!DOCTYPE HTML>
<!--[if lt IE 7 ]>				<html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>					<html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>					<html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>					<html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->	<html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700,300italic,400italic,500italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" />
	<script src="<?php bloginfo('template_url'); ?>/js/modernizr-2.0.6.js"></script>
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/vnd.microsoft.icon">
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
	<?php	wp_head(); ?>
	<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.sexyDrops.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.sexyPlaceholders.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/tinynav.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/script.js"></script>
</head>
<body <?php body_class(); ?>>
<div id="wrapper"><div id="wrapperHome">
<div class="container">
<div id="headWrapper">
<header id="mainHeader">
	<a id="logo-main" href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-main.png" alt ="Quick Cash Advance" /></a>
	<form method="get" action="/wordpress/" id="hSearch">
		<input type="text" value="" placeholder="Type your search here..." name="s" id="hsBox">
		<input type="submit" value="" id="hsButton">
	</form>
</header>
<nav id="mainMenu">
	<?php wp_nav_menu(array('theme_location' => 'main-nav', 'container' => false, 'items_wrap' => '<ul>%3$s</ul>')); ?>
</nav>
</div><!-- headWrapper -->
<section id="subheader">
	<?php if(is_front_page()) { ?>
		<div id="shSteps">
			<h2>Quick, <span>Easy</span> Cash</h2>
			<p id="step1" class="stepBlocks">Complete your simple and short cash request form</p>
			<p id="step2" class="stepBlocks">Get matched with top-notch lenders from our exclusive network</p>
			<p id="step3" class="stepBlocks">Pick your offer and get the cash you need ASAP</p>
		</div>
	<?php } ?>
<div id="qrForm">
	<h2>Start Your Request</h2>
	<div id="formPlacement">
	<form action="/cash-advance-form/" method="GET" id="homepageMiniForm">
		<div class="formLeft firstLine">
			<label>Loan Amount</label>
			<select class="normal" name="amount">
				<option value="100"  >$100</option>
				<option value="200"  >$200</option>
				<option value="300"  >$300</option>
				<option value="400"  >$400</option>
				<option value="500" selected="selected">$500</option>
				<option value="600"  >$600</option>
				<option value="700"  >$700</option>
				<option value="800"  >$800</option>
				<option value="900"  >$900</option>
				<option value="999"  >$1000+</option>
			</select>
		</div>
		<div class="formRight firstLine" id="zipRight">									
			<label>Zip Code</label>
			<span class="field-holder"><input type="tel" name="zip" class="field" placeholder="Zip Code" value="" title="Zip Code" /></span>
		</div>
		<div class="formLeft secondLine" id="formName">
		<label>First Name</label>
		<span class="field-holder"><input type="text" class="field" name="fName" placeholder="First Name" value="" title="First Name" /></span>
		</div>
		<div id="fLast" class="formRight secondLine">
		<label>Last Name</label>
		<span class="field-holder"><input type="text" class="field" name="lName" placeholder="Last Name" value="" title="Last Name" /></span>
		</div>
		<div class="thirdLine">
		<label>Email Address</label>
		<span class="field-holder-large"><input type="text" name="email" class="field" placeholder="name@example.com" value="" title="E-Mail Address" /></span>
		</div>
		<span class="button-holder"><input type="submit" class="submit-button" value="" /></span>
	</form>
	</div><!-- formPlacement -->
	<p>By submitting your information you understand and agree to our <a href="#">Privacy Policy</a> and <a href="#">Terms of Use</a>.</p>
	</div><!-- qrForm -->
</section>