<?php
/*
Template Name: Archives
*/
get_header(); ?>
<section id="contentArea">
<?php get_sidebar(); ?>
<div id="contentWrapper">
<article id="mainContent" class="contentBox">

<div id="container">
	<div id="content" role="main">

		<?php the_post(); ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		
		<div id="archiveSearch"><?php get_search_form(); ?></div>

		<div class="lcCats">
			<ul>
				 <?php wp_list_categories(); ?>
			</ul>
		</div>

	</div><!-- #content -->
</div><!-- #container -->
<?php get_footer(); ?>