<?php session_start(); ?>
<!DOCTYPE HTML>
<!--[if lt IE 7 ]>				<html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>					<html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>					<html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>					<html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->	<html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700,300italic,400italic,500italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" />
	<script src="<?php bloginfo('template_url'); ?>/js/modernizr-2.0.6.js"></script>
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/vnd.microsoft.icon">
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
	<?php	wp_head(); ?>
	<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.sexyDrops.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.sexyPlaceholders.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/tinynav.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/script.js"></script>
</head>
<body class="apply">
<div id="wrapper"><div id="wrapperHome">
<div class="container">
<div id="headWrapper">
<header id="mainHeader">
	<a id="logo-main" href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-main.png" alt ="Quick Cash Advance" /></a>
</header>

</div><!-- headWrapper -->
<section id="contentArea">
<div id="contentWrapper">
<article id="mainContent" class="contentBox">
		<?php if ( have_posts() ) :
			while ( have_posts() ) :
				the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
<?php get_footer(); ?>