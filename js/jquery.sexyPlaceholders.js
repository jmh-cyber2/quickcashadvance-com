/*	
	Author: Jason Stockman @ Cyber2Media
	Date: Oct 31, 2012
	Version: 1.0
	------------------------------------

	Applies this placeholder value to the value of the input.
	Clears the input if on focus if the value is original (no custom value entered)
	Refills the input with the original if the value is empty on blur.
	
*/

(function($){  

	jQuery.support.placeholder = (function(){
	    var i = document.createElement('input');
	    return 'placeholder' in i;
	})(); 

	$.fn.sexyPlaceholders = function(options) {
		
		// Apply styles and class
		$(this).each( function(e) {

			var e = $(this);
			var orig = e.attr('placeholder');
			
			e.addClass('sexyPlaceholders'); 
			e.attr('placeholder','');
			e.attr('value',e.attr('value') || orig);
			e.focus(function() {
				if ( $(this).val()==orig )
					$(this).attr('value','')
			});
			e.blur(function() {
				if ( $(this).val()=='' )
					$(this).attr('value',orig)
			});

		});

	};  

})(jQuery); 