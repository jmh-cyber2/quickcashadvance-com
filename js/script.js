$(document).ready(function() {

	$('select').sexyDrops();
	
	// adds tinyNav for use in mobile browsers
	$("#mainMenu ul").tinyNav({header:'Navigation'});

	// sliding menu dropdown
	var dropDown = $('nav#mainMenu > ul > li > ul').parent('li'); // create a variable of the parent li of all submenus
	var dropMenu = $('nav#mainMenu > ul > li > ul'); // the submenus
	dropDown.addClass('showDrop');  // add .showDrop to every parent li of submenus
	dropDown.children('a').click(function(e){
		e.preventDefault();
	}); // when parent li is clicked, do nothing (do not go to a page)
	dropDown.hover(
		function(){
			dropMenu.slideDown('fast');
		},
		function(){
			dropMenu.slideUp('fast');
		}); // when you hover on the parent li: slide the submenu down, when your mouse leaves the area: slide the submenu back up
	
	
	// sexyPlaceholders
	$('input').sexyPlaceholders();

	// email validation
	function validateEmail($email) {
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if( !emailReg.test( $email ) ) {
			return false;
	  	} else {
			return true;
	  	}
	}

	// submission of the home page mini form
	$('#homepageMiniForm').submit(function() {

			// Validation
			var zipcode = $('input[name="zip"]').val();
			var email = $('input[name="email"]').val();
			var fname = $('input[name="fName"]').val();
			var lname = $('input[name="lName"]').val();

			if (zipcode==""||zipcode=="Zip Code") {
				alert("Please enter your zipcode.");
				return false;
			}
			else if (fname==""||fname=="First Name") {
				alert("Please enter your first name.");
				return false;
			}
			else if (lname==""||lname=="Last Name") {
				alert("Please enter your last name.");
				return false;
			}							
			else if (email==""||email=="name@example.com") {
				alert("Please enter your e-mail address.");
				return false;
			}
			else if (!validateEmail(email)) {
				alert("Please enter a valid e-mail address.");
				return false;
			}
			location.href = 'http://jmh.dev.640.org/wordpress/cash-advance-form?' + $('#homepageMiniForm').serialize();
			return false;
	});
	
	$('#notaccordion div').hide();
	// ACCORDION
	$('#notaccordion h3').click( function() {
		// The H3 that was clicked
		var h3 = $(this);
		// Find the index
		var dex = $('#notaccordion h3').index(this);
		// Find the DIV we want
		var div = $('#notaccordion div').eq(dex);
		// We show/hide the DIV
		if ( h3.hasClass('open') ) {
			div.stop(1,1).slideUp(200);
			h3.removeClass('open');
		}
		else {
			$('#notaccordion div').slideUp(200);
			$('#notaccordion h3').removeClass('open');
			div.stop(1,1).slideDown(200);
			h3.addClass('open');
		}
		return false;
	});
	
	/* enables button on quick request form to change designs between page 1 and page 2 */
	$('.next-button').click(function(){$(this).addClass('page2');});
	$('.prev-button').click(function(){
		$('.next-button').attr('style', '');
		$('.next-button').removeClass('page2');
	});
	
	// if the window is less than or equal to 600px, rearrange items
	function handleResize() {
		if($(window).width() <= 600 && !$('body').hasClass('home')) {
			$('#contentArea').insertAfter('#mainMenu');
			$('#contentWrapper').insertBefore('#sbWrapper');
			$('#qrForm').insertAfter('#contentWrapper');
		} 
		else {
			$('#contentArea').insertAfter('#subheader');
			$('#contentWrapper').insertAfter('#sbWrapper');
			$('#qrForm').appendTo('#subheader');
		}
	}
	handleResize(); // call function on document load
	$(window).resize(handleResize); // call function on window resize
});
