/*	
	Author: Jason Stockman @ Cyber2Media
	Date: Oct 16, 2012
	Version: 1.0
	------------------------------------

	Hides the select using 0 opacity, then layers it with a div using position:absolute and z-index
	Sets the html() of the new div to the selected option for the select
	Updates the html() of the div to the newly selected option onChange of the select 
	Calculates the width of the original select element and applies it to the new div using inline CSS
	Recalculates this width on window.resize in case responsive CSS is applied to the original select for some reason

*/

(function($){  


	$.fn.sexyDrops = function(options){

		//options = $.extend( {padding: 10, borderWidth: 1}, options);

		// Apply styles and class
		this.each( function(e) {

			//setup and styling

			var e = $(this);
			var w = e.width();
			var v = e.find('option:selected').text();
			var opts = e.find('option');

			e.css({opacity:0})
			e.addClass('sexyDrop').wrap('<div class="sexyDropWrapper">');
			e.parent('.sexyDropWrapper').prepend('<span class="sexyDropUI">'+v+'</span>');
			//bind functionality
			e.change(function() {
				var v = e.find('option:selected').text();
				var UI = e.parent('.sexyDropWrapper').find('.sexyDropUI');
				UI.html(v);
			});

			/*
			$(window).resize( function() {
				e.each( function() {
					//setup and styling
					var w = $(this).outerWidth();
					var wrapper = $(this).parent('.sexyDropWrapper');
					var UI = $(this).parent('.sexyDropWrapper').find('.sexyDropUI');
					UI.css({width: (w)});
					wrapper.css({width: w});
				});
			});
			*/
		});

	};  

})(jQuery); 
