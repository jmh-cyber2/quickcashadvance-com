<?php
/*
Template Name: Category
 */

get_header(); ?>
<section id="contentArea">
<?php get_sidebar(); ?>
<div id="contentWrapper">
<article id="mainContent" class="contentBox">

	<!-- BEGIN category.php -->


		<?php the_post(); ?>

		<h1 class="page-title"><?php _e( 'Learning Center:', 'quickcashadvance' ) ?> <span class="category-title"><?php single_cat_title() ?></span></h1>
		<?php $categorydesc = category_description(); if ( !empty($categorydesc) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . $categorydesc . '</div>' ); ?>
		<?php rewind_posts(); ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<h2 class="categoryLinks"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="postExcerpt"><?php the_excerpt(); ?></div>
		<?php endwhile; ?>

	<!-- END category.php -->

<?php get_footer(); ?>