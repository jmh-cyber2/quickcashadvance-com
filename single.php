<?php
/**
 * The Template for displaying all single posts.
 */
get_header(); ?>

<section id="contentArea">
<?php get_sidebar(); ?>
<div id="contentWrapper">
<article id="mainContent" class="contentBox">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>