<?php

// START Register Navigations
register_nav_menus (
	array (
		'main-nav' => 'Main Navigation',
		'footer-nav' => 'Footlinks Navigation'
	)
);
// END Register Navigations

// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
       global $post;
	return '... <a class="moretag" href="'. get_permalink($post->ID) . '">read more</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

if (function_exists('register_sidebar'))

{

    // Define Sidebar Widget Area 1

    register_sidebar(array(

        'name' => __('Sidebar', 'quickcashadvance'),

        'description' => __('The widgets placed here will show up on the sidebar', 'quickcashadvance'),

        'id' => 'widget-area',

        'before_widget' => '<div id="%1$s" class="%2$s">',

        'after_widget' => '</div>',

        'before_title' => '<h2>',

        'after_title' => '</h2>'

    ));
    
	}

?>