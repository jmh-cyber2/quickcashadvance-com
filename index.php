<?php get_header(); ?>
<section id="contentArea">
<?php get_sidebar(); ?>
<div id="contentWrapper">
<article id="mainContent" class="contentBox">
	<?php if(have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
	
		<div <?php post_class(); ?>>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			
			<?php the_content(''); ?>
			
			<div class="post-info">
				<ul>
					<li class="date"><?php the_time('jS F Y'); ?></li>
					<li class="category">Posted in <?php the_category(', '); ?></li>
					<li class="read-more"><a href="<?php the_permalink(); ?>">Read more</a></li>
				</ul>
			</div>
			
			<?php endwhile; ?>
			
			<div class="pagination">
				<p class="prev"><?php next_posts_link('Older articles'); ?></p>
				<p class="next"><?php previous_posts_link('Newer articles'); ?></p>
			</div>
			
			<?php endif; ?>
		</div>
<?php get_footer(); ?>