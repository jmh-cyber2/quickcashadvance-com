<?php get_header(); ?>
<section id="contentArea">
<?php get_sidebar(); ?>
<div id="contentWrapper">
<article id="mainContent" class="contentBox">
	<h1>Sorry!</h1>
	<p>This page does not exist. <a href="javascript:history.back();">&laquo; BACK</a></p>
<?php get_footer(); ?>