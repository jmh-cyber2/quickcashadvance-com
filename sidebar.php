<!-- BEGIN sidebar.php -->
<div id="sbWrapper">
	<section id="sidebar" class="contentBox">
		<?php if(function_exists('dynamic_sidebar')) {
			dynamic_sidebar('widget-area');
		}?>
	</section><!-- sidebar -->
</div>
<!-- END sidebar.php -->